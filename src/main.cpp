/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <vector>
#include <unordered_map>
#include <thread>

#include "tglib.h"

using namespace tgl;

class Game
{
    public:
        enum class grid_state : int8_t { empty = 0, player_1, player_2 };

    public:

        void setup()
        {
            _create_sprites();
            _player_message.present();
        }

        void update()
        {
            _update_input();
            _check_win();
        }

        void draw()
        {
            fill( _surface, Color::none );

            blit( _surface, _sprites["grid"], { 0, 0 } );
            blit( _surface, _sprites["cursor"], _cursor_position * 4 );

            _draw_state();

            _surface.present();
        }

        void reset_state()
        {
            for( auto &s : _grid_states ) { s = grid_state::empty; }

            _player_1 = _winner == grid_state::player_2;

            _update_player_msg();

            _winner = grid_state::empty;
            _win_message.clear();
        }

        bool quit() const noexcept { return _quit; }

    private:
        void _update_player_msg()
        {
            _player_message.set_text( _player_1 ? "Player 1" : "Player 2" );
            _player_message.present();
        }

        void _draw_state()
        {
            Vec2 pos{ 0, 0 };

            auto &os = _sprites["o"];
            auto &xs = _sprites["x"];

            for( auto gs : _grid_states )
            {
                if( gs == grid_state::player_1 )
                {
                    blit( _surface, xs, pos );
                }
                else if( gs == grid_state::player_2 )
                {
                    blit( _surface, os, pos );
                }

                pos.x += 4;
                if ( pos.x > 8 )
                {
                    pos = { 0, pos.y + 4 };
                }
            }
        }

        void _create_sprites()
        {
            Color_Pair  yellow{ Color::yellow };
            Color_Pair  black{ Color::black };

            auto&   y = yellow;
            auto&   b = black;
            _sprites.emplace( "grid", Bitmap{{
                { b, b, b, y, b, b, b, y, b, b, b },
                { b, b, b, y, b, b, b, y, b, b, b },
                { b, b, b, y, b, b, b, y, b, b, b },

                { y, y, y, y, y, y, y, y, y, y, y },

                { b, b, b, y, b, b, b, y, b, b, b },
                { b, b, b, y, b, b, b, y, b, b, b },
                { b, b, b, y, b, b, b, y, b, b, b },

                { y, y, y, y, y, y, y, y, y, y, y },

                { b, b, b, y, b, b, b, y, b, b, b },
                { b, b, b, y, b, b, b, y, b, b, b },
                { b, b, b, y, b, b, b, y, b, b, b },

            }} );

            _sprites.emplace( "mask", Bitmap{ 12, 12, Color::none });

            Color_Pair  blue{ Color::blue };
            _sprites.emplace( "x", Bitmap{{
                { blue, black, blue },
                { black, blue, black },
                { blue, black, blue }
            }} );

            Color_Pair  green{ Color::green };
            _sprites.emplace( "o", Bitmap{{
                { black, green, black },
                { green, black, green },
                { black, green,  black }
            }} );

            Color_Pair red{ Color::red };
            _sprites.emplace( "cursor", Bitmap{{
                { red, red, red },
                { red, red, red },
                { red, red, red }
            }} );
        }

        void _update_input()
        {
            _keyboard.update();

            // Quit
            if( _keyboard.pressed( KEY_END ) || _keyboard.pressed( 'q' ) )
            {
                _quit = true;
                return;
            }

            // Rest Game
            if( _keyboard.pressed( 'r' ) )
            {
                reset_state();
            }

            // Navigate Board
            if( _winner == grid_state::empty )
            {
                if( _keyboard.pressed( KEY_LEFT ) ) { if( _cursor_position.x ) --_cursor_position.x; }
                if( _keyboard.pressed( KEY_RIGHT ) ) { if( _cursor_position.x < 2 ) ++_cursor_position.x; }
                if( _keyboard.pressed( KEY_UP ) ) { if( _cursor_position.y ) --_cursor_position.y; }
                if( _keyboard.pressed( KEY_DOWN ) ) { if( _cursor_position.y < 2 ) ++_cursor_position.y; }

                if( _keyboard.pressed( ' ' ) || _keyboard.pressed( '\n' ) )
                {
                    auto &gi = _grid_states[ _cursor_position.x + _cursor_position.y * 3 ];
                    if( gi == grid_state::empty )
                    {
                        gi = _player_1 ? grid_state::player_1 : grid_state::player_2;
                        _player_1 = !_player_1;
                        _update_player_msg();
                    }
                }
            }
        }

        bool _win_row_check( int8_t start, int8_t step, grid_state compare ) const noexcept
        {
            const auto &states = _grid_states;
            const auto last = step + step;
            return states[start] == compare && states[start + step] == compare && states[start + last] == compare;
        };

        template<int N>
        void _check_win( int step, const int (&points)[N] )
        {
            auto cmp = _grid_states[points[0]];
            if( cmp == grid_state::empty ) { return; }

            for( auto i : points )
            {
                if( _win_row_check( i, step, cmp ) ) { _winner = cmp; break; }
            }
        }

        void _check_win()
        {
            if( _winner != grid_state::empty ) { return; }

            _check_win( 1, { 0, 3, 6 } );
            _check_win( 3, { 0, 1, 2 } );
            _check_win( 4, { 0 } );
            _check_win( 2, { 2 } );

            if( _winner == grid_state::player_1 )
            {
                _win_message.set_text( "Player 1 Wins", Msg_Box::center );
                _win_message.present();
            }
            else if( _winner == grid_state::player_2 )
            {
                _win_message.set_text( "Player 2 Wins", Msg_Box::center );
                _win_message.present();
            }
        }

    private:
        bool    _quit{ false };

        NCurses  _nc{};
        Surface _surface{ 12, 12, 0, 0 };

        Keyboard _keyboard{};

        Msg_Box      _win_message{ 22, "", { 0, 15 }, Msg_Box::center };
        Msg_Box      _player_message{ 22, "Player 1", { 0, 12 }, Msg_Box::center };

        bool         _player_1{ true };
        grid_state   _winner{ grid_state::empty }; 

        Vec2         _cursor_position{ 0, 0 };

        std::unordered_map<std::string, Bitmap>   _sprites;

        std::array<grid_state, 9> _grid_states{ grid_state::empty };
};

int main()
{
    Game    game{};

    game.setup();
    while( !game.quit() )
    {
        game.update();

        game.draw();

        std::this_thread::sleep_for( std::chrono::milliseconds( 34 ) );
    }

    auto key = getch();

    return 0;
}
/**/
