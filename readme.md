### Tic-tac-toe
Tic-tac-toe game that runs in terminal using ncurses.  (No AI)

![Running](./docs/screen.png?raw=true "Running")  

### Clone
```bash
$ git clone --recursive https://gitlab.com/_mobius/terminal-ttt.git
```

### Installing depenendencies  
```bash
$ apt install libncurses5-dev libncursesw5-dev
```

### Compiling  
```bash
$ mkdir build  
$ cd build  
$ cmake -DCMAKE_BUILD_TYPE=Release ..  
$ cmake --build .  
```
### Controls  
* _move_: __arrows__
* _select_: __space__
* _quit_: __q__
